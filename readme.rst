Proyecto de extracción de datos de cursos del INAEM
====================================================

El INAEM muestra su listado de cursos en "https://plan.aragon.es/MapaRec.nsf/fmrListado".

El proyecto requiere scrapy

Extracción de datos:

.. code:: bash

  $ scrapy crawl -o inaem.json -t json inaem


Recuerda que para instalar scrapy

.. code:: bash

  $ sudo apt-get install libxslt1-dev
  $ sudo apt-get install libz-dev
  $ sudo apt-get install libffi-dev
  $ sudo apt-get install libssl-dev
  $ virtualenv env
  $ source env/bin/activate
  $ pip install scrapy



TODO
=====
* celery + scrapy http://stackoverflow.com/questions/11528739/running-scrapy-spiders-in-a-celery-task
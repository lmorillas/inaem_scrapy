from scrapy.spider import Spider
from scrapy.selector import Selector
from inaem.items import Curso
from urlparse import urljoin
from scrapy.http import Request
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.loader.processor import MapCompose, Join, TakeFirst



class InaemSpider(Spider):
    name = "inaem"
    allowed_domains = ["aragon.es"]
    start_urls = [
        "https://plan.aragon.es/MapaRec.nsf/fmrListado",
    ]

    def parse(self, response):
        sel = Selector(response)
        cursos = sel.xpath('//tr[td/@class="textoApl1"]')
        lcursos = []
        for curso in cursos:
            c = self.parse_cursoit(curso)
            lcursos.append(c)
        return lcursos


    def parse_cursoit(self, curso):
        c = ItemLoader(item=Curso(), selector=curso)
        c.default_input_processor = MapCompose(lambda string: string.strip())
        c.default_output_processor = lambda x: x[0] if x else ""
        c.add_xpath('nombre', 'td/a/text()')
        c.add_xpath('modalidad', 'td[2]/text()')
        c.add_xpath('alumnos', 'td[3]/text()')
        c.add_xpath('horas', 'td[4]/text()')
        c.add_xpath('localidad', 'td[5]/text()')
        c.add_xpath('fecha', 'td[6]/text()')


        request = Request(urljoin("https://plan.aragon.es/",
                          curso.xpath('td/a/@href').extract()[0]),
                            callback = self.parse_curso_detalle,
                            meta={'item': c.load_item()})
        return request


    def parse_curso_detalle(self, response):
        _item = response.meta['item']
        _item['url'] = response.url
        sel = Selector(response)

        item = ItemLoader(item = _item, selector = sel)

        item.default_input_processor = MapCompose(lambda string: string.strip())
        item.default_output_processor = lambda x: x[0] if x else ""
        item.add_xpath('url_preinscr', u'//a[contains(@href, "Preinscripcion")]/@href')
        #item.add_xpath('num_preinscr'] = sel.xpath('//div[contains(., "Preinscripciones realizadas")]/span/text()')
        item.add_xpath('entidad', '//div[contains(div/label, "Entidad")]/div[2]/text()')
        item.add_xpath('centro', '//div[contains(div/label, "Centro")]/div[2]/text()')
        item.add_xpath('familia', '//input[@name="NomFam"]/@value')
        item.add_xpath('mes_inicio', '//input[@name="MesInicio"]/@value')
        item.add_xpath('requisitos', '//input[@name="Requisitos"]/@value')
        item.add_xpath('requisitos_notas', '//input[@name="RequisitosNotas"]/@value')
        item.add_xpath('finalidad', '//input[@name="Finalidad"]/@value')
        item.add_xpath('num_preinscr', '//input[@name="Preinscritos"]/@value')
        item.add_xpath('fecha_inicio', '//input[@name="FhInicio"]/@value')
        item.add_xpath('horario', '//input[@name="Horario"]/@value')
        item.add_xpath('certif_prof', '//label[@for="CertProf"]/text()')
        item.add_xpath('fecha_fin', '//input[@name="FhFin"]/@value')
        item.add_xpath('direccion', '/html/body/form/fieldset[2]/div[3]/div[2]/text()')
        return item.load_item()


'''
Mirar http://doc.scrapy.org/en/latest/topics/loaders.html

https://github.com/redapple/parslepy/wiki/Use-parslepy-with-scrapy
https://raw.githubusercontent.com/redapple/parslepy/master/parslepy/utils/scrapytools.py

'''



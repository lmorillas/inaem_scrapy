# -*- coding: utf-8 -*-
# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib.loader.processor import MapCompose, Join, TakeFirst
from urlparse import urljoin

def parse_cert_prof(x):
    return "Sí" if x else "No"


primero = lambda x: x[0] if x else ""


def url_completa(url):
    if url:
        return urljoin("https://plan.aragon.es/", url)


class Curso(Item):
    # define the fields for your item here like:
    # name = Field()
    #default_input_processor = MapCompose(lambda v: v.sstrip(), TakeFirst)

    nombre = Field(
                   input_processor = MapCompose(lambda string: string.title())
                   )
    modalidad = Field()
    alumnos = Field()
    horas = Field()
    localidad = Field()
    fecha = Field()
    url_preinscr = Field(input_processor = MapCompose(url_completa))
    entidad = Field()
    centro = Field()
    familia = Field()
    mes_inicio = Field()
    requisitos = Field()
    requisitos_notas = Field()
    fecha_inicio = Field()
    fecha_fin = Field()
    num_preinscr = Field()
    horario = Field()
    direccion = Field()
    certif_prof = Field(
                        input_processor = MapCompose(parse_cert_prof)
                        )
    finalidad = Field()
    url = Field()

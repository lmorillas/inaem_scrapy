#!/bin/sh

RUTA=/home/lm/src/inaem_scrapy
DESTINO=/home/lm/src/inaem-exhibit/
cd $RUTA

. $RUTA/env/bin/activate

rm inaem.json

scrapy crawl -o inaem.json -t json inaem
cp inaem.json $DESTINO

